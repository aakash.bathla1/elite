import { Observable } from 'rxjs/Observable';
import { Component, ViewChild, ElementRef } from '@angular/core';
import { IonicPage, NavController, NavParams, Loading, LoadingController } from 'ionic-angular';
import { UserService } from '../../providers/user';
import * as $ from 'jquery';



@IonicPage()
@Component({
  selector: 'page-progress',
  templateUrl: 'progress.html',
})
export class ProgressPage {
  public items: Observable<any[]>;
  public loader: Loading;
  @ViewChild('file') fileInput: ElementRef;

  private currentItem: any;

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public userService: UserService,
    private loadingCtrl: LoadingController
  ) {
  }
  ionViewWillEnter() {
    this.items = this.userService.getProgress();

    $(document).ready(function(){
      $('.progress-list').on('click', '.uploaded-image img', function(){
        $(this).parents().eq(5).addClass('active');
      });
      $('.progress-list').on('click', '.close', function(){
        $('.progress-image').removeClass('active');
      });
      $('.progress-list').on('click', '.next', function(){
        $(this).parents().eq(2).removeClass('active');
        $(this).parents().eq(2).next().addClass('active');
      });
      $('.progress-list').on('click', '.previous', function(){
        $(this).parents().eq(2).removeClass('active');
        $(this).parents().eq(2).prev().addClass('active');
      });
    });
  }

  onCurrentWeekChanged(week: number) {
    // do nothing for now
  }

  showFileBrowser(item: any): void {
    this.currentItem = item;
    this.fileInput.nativeElement.click();
  }

  onFileChange(files:FileList): void {
    console.log(files);
    this.loader = this.loadingCtrl.create();
    this.loader.present();
    this.userService.uploadProgressPhoto(files[0], this.currentItem.week)
      .subscribe(() => {
        this.items = this.userService.getProgress();
        this.loader.dismiss();
      }, error => this.loader.dismiss())
  }
}
