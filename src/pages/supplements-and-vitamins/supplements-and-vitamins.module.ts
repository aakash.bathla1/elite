import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { SupplementsAndVitaminsPage } from './supplements-and-vitamins';

@NgModule({
  declarations: [
    SupplementsAndVitaminsPage,
  ],
  imports: [
    IonicPageModule.forChild(SupplementsAndVitaminsPage),
  ],
})
export class SupplementsAndVitaminsPageModule {}
