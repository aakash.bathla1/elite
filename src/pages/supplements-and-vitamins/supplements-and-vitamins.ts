import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';

/**
 * Generated class for the SupplementsAndVitaminsPage page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-supplements-and-vitamins',
  templateUrl: 'supplements-and-vitamins.html',
})
export class SupplementsAndVitaminsPage {
  public supplementsRoot: any = "SupplementsPage";
  public vitaminsRoot: any = "VitaminsPage";

  constructor(public navCtrl: NavController, public navParams: NavParams) {
  }

}
