import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';

import { UserService } from './../../providers/user';

@IonicPage()
@Component({
  selector: 'page-supplements',
  templateUrl: 'supplements.html',
})
export class SupplementsPage {
  constructor(
    public navCtrl: NavController, 
    public navParams: NavParams,
    private userService: UserService,
  ) {
  }

  gotoHomePage(): void {
    this.userService.resetNavigationTo('home');
  }

}
