import { Component } from '@angular/core';
import { IonicPage, NavController, Loading, LoadingController } from 'ionic-angular';

import { UserService } from '../../providers/user';
import { User } from './../../shared/user';
import * as $ from 'jquery';

@IonicPage()
@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {
  public loader: Loading;
  public weeks: any[];
  public currentWeek: number = 1;
  public showGettingStarted: boolean;
  constructor(
    public navCtrl: NavController,
    private userService: UserService,
    public loadingCtrl: LoadingController
  ) {
    this.currentWeek = this.userService.currentWeek;
    this.showGettingStarted = false;
  }

  ionViewWillEnter() { 
    this.userService.getSelf()
      .subscribe((user: User) => this.showGettingStarted = user.showGettingStarted)

    $('.week-label').each(function() {
        var text = $(this).text();
        $(this).text(text.replace('Week 0', 'free trial week')); 
    });
    $('.section-title').each(function() {
        var text = $(this).text();
        $(this).text(text.replace('Week 0', 'your free trial week')); 
    });
  }


  onCurrentWeekChanged(week: number) {
    this.currentWeek = week;
    this.navCtrl.push('HomePage', { currentWeek: this.currentWeek});
  }

  gotoOverview() {
    this.navCtrl.push('OverviewPage', { currentWeek: this.currentWeek}).then(() => this.loader.dismiss());
    
    this.loader = this.loadingCtrl.create();
    this.loader.present();
  }

  gotoTraining() {
    this.navCtrl.push('TrainingPage', { currentWeek: this.currentWeek}).then(() => this.loader.dismiss());
    
    this.loader = this.loadingCtrl.create();
    this.loader.present();
  }
  
  gotoWorkouts() {
    this.navCtrl.push('WorkoutsPage', { currentWeek: this.currentWeek, currentDay: 1}).then(() => this.loader.dismiss());
    
    this.loader = this.loadingCtrl.create();
    this.loader.present();
  }

  gotoNutrition() {
    this.navCtrl.push('NutritionPage', { currentWeek: this.currentWeek}).then(() => this.loader.dismiss());
    
    this.loader = this.loadingCtrl.create();
    this.loader.present();
  }

  gotoGettingStarted() {
    this.navCtrl.push('GettingStartedPage').then(() => this.loader.dismiss());
      
    this.loader = this.loadingCtrl.create();
    this.loader.present();
  }

  gotoTrackYourProgress() {
    this.navCtrl.push('ProgressPage').then(() => this.loader.dismiss());
    
    this.loader = this.loadingCtrl.create();
    this.loader.present();
  }

  gotoEducation() {
    this.navCtrl.push('EducationPage').then(() => this.loader.dismiss());
    
    this.loader = this.loadingCtrl.create();
    this.loader.present();
  }

  gotoExercises() {
    this.navCtrl.push('ExercisesPage').then(() => this.loader.dismiss());
    
    this.loader = this.loadingCtrl.create();
    this.loader.present();
  }

  gotoSupplements() {
    this.navCtrl.push('SupplementsAndVitaminsPage').then(() => this.loader.dismiss());
    
    this.loader = this.loadingCtrl.create();
    this.loader.present();
  }

  gotoElite() {
    this.navCtrl.push('ElitePage').then(() => this.loader.dismiss());
    
    this.loader = this.loadingCtrl.create();
    this.loader.present();
  }

  gotoCoaching() {
    this.navCtrl.push('CoachingPage').then(() => this.loader.dismiss());
    
    this.loader = this.loadingCtrl.create();
    this.loader.present();
  }

  gotoAccountability() {
    this.navCtrl.push('AccountabilityPage').then(() => this.loader.dismiss());
    
    this.loader = this.loadingCtrl.create();
    this.loader.present();
  }
}
