import { Component, ElementRef, ViewChild } from '@angular/core';
import { IonicPage, NavParams, LoadingController, Loading, NavController } from 'ionic-angular';
import { Validators, FormBuilder, FormGroup } from '@angular/forms';

import { UserService } from './../../providers/user';
import { User } from './../../shared/user';

@IonicPage()
@Component({
  selector: 'page-profile',
  templateUrl: 'profile.html',
})
export class ProfilePage {
  public user: User;
  public profileForm: FormGroup;
  public loader: Loading;
  @ViewChild('file') fileInput: ElementRef;

  constructor(
    public navCtrl: NavController, 
    public navParams: NavParams,
    public userService: UserService,
    private fb: FormBuilder,
    private loadingCtrl: LoadingController
  ) {
    this.user = new User();
    if (navParams.get('user')) {
      this.user = navParams.get('user');
    }
    this.profileForm = this.fb.group({
      email: [this.user.email, Validators.required],
      name: [this.user.name, Validators.required],
      height: [this.user.height],
      startingWeight: [this.user.startingWeight],
      currentWeight: [this.user.currentWeight],
      goalWeight: [this.user.goalWeight],
      age: [this.user.age],
      timezone: [this.user.timezone]
    });
  }

  showFileBrowser(): void {
    this.fileInput.nativeElement.click();
  }

  onFileChange(files:FileList): void {
    console.log(files);
    this.loader = this.loadingCtrl.create();
    this.loader.present();
    this.userService.uploadPhoto(files[0])
      .subscribe((user: User) => {
        this.user = user;
        this.loader.dismiss();
      }, error => this.loader.dismiss())
  }

  onSubmit() {
    console.log(this.profileForm.value);
    this.userService.updateProfile(this.profileForm.value)
      .subscribe(user => {
        this.loader.dismiss();
        this.navCtrl.push('AccountPage');
      })
      ;

    this.loader = this.loadingCtrl.create();
    this.loader.present();
  }

}
