import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { EducationSinglePage } from './education-single';

import { ComponentsModule } from '../../components/components.module';

@NgModule({
  declarations: [
    EducationSinglePage,
  ],
  imports: [
    IonicPageModule.forChild(EducationSinglePage),
    ComponentsModule,
  ],
})
export class EducationSinglePageModule {}
