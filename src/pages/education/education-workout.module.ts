import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { EducationWorkoutPage } from './education-workout';

import { ComponentsModule } from '../../components/components.module';

@NgModule({
  declarations: [
    EducationWorkoutPage,
  ],
  imports: [
    IonicPageModule.forChild(EducationWorkoutPage),
    ComponentsModule,
  ],
})
export class EducationWorkoutPageModule {}
