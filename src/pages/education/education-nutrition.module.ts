import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { EducationNutritionPage } from './education-nutrition';

import { ComponentsModule } from '../../components/components.module';

@NgModule({
  declarations: [
    EducationNutritionPage,
  ],
  imports: [
    IonicPageModule.forChild(EducationNutritionPage),
    ComponentsModule,
  ],
})
export class EducationNutritionPageModule {}
