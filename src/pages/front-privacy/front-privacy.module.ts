import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { FrontPrivacyPage } from './front-privacy';

@NgModule({
  declarations: [
    FrontPrivacyPage,
  ],
  imports: [
    IonicPageModule.forChild(FrontPrivacyPage),
  ],
})
export class FrontPrivacyPageModule {}
