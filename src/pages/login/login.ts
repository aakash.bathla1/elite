import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, LoadingController, Loading, ToastController } from 'ionic-angular';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Storage } from '@ionic/storage';

import { UserService } from '../../providers/user';

@IonicPage()
@Component({
  selector: 'page-login',
  templateUrl: 'login.html',
})
export class LoginPage {
  public login: FormGroup;
  public loader: Loading;
  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    private fb: FormBuilder,
    private loadingCtrl: LoadingController,
    private toastCtrl: ToastController,
    private userService: UserService,
    private storage: Storage
  ) {
    this.login = this.fb.group({
      email: ['', Validators.required],
      password: ['', Validators.required]
    });
  }

  ionViewDidEnter() {

  }

  onSubmit() {
    if (this.login.invalid) {
      let error = 'Unable to proceed. All fields are required.';
      let info = this.toastCtrl.create({
        message: error,
        position: 'top',
        duration: 3000
      });
      info.present();

      return;
    }
    this.userService.login(this.login.value.email, this.login.value.password)
      .subscribe((user) => {
        console.log('Login :: user', JSON.stringify(user))
        this.storage.get('subscription').then(subscription => {
          console.log('Login :: subscription', JSON.stringify(subscription))
          this.loader.dismiss();
          // if (subscription ) {
          //   if ((subscription.weekly || subscription.unlimited)) {
          //     console.log("subscription found");
          //     let info = this.toastCtrl.create({
          //       message: "Subscription found..",
          //       position: 'top',
          //       duration: 3000
          //     });
          //     info.present();
          //     this.userService.resetNavigationTo('home')
          //   } else {
          //     console.log("subscription not valid NOT found take to subscription page");
          //     let info = this.toastCtrl.create({
          //       message: "No valid subscription found",
          //       position: 'top',
          //       duration: 3000
          //     });
          //     info.present();
          //     this.userService.resetNavigationTo('subscription')
          //   }
          // }
          // else {
          console.log("Goto to subscription page so we can check sub status for now..");
          // let info = this.toastCtrl.create({
          //   message: "No subscription found",
          //   position: 'top',
          //   duration: 3000
          // });
          // info.present();
          this.userService.resetNavigationTo('subscription')
          // }
        })
      }, error => {
        console.log("could not login error");
        this.loader.dismiss();
        let info = this.toastCtrl.create({
          message: error.message,
          position: 'top',
          duration: 3000
        });
        info.present();
      });

    this.loader = this.loadingCtrl.create({ dismissOnPageChange: true });
    this.loader.present();
  }

  gotoPasswordResetPage(e) {
    e.preventDefault();
    this.navCtrl.push('PasswordResetPage')
  }

  gotoFrontPage(e) {
    e.preventDefault();
    this.navCtrl.push('FrontPage')
  }

}
