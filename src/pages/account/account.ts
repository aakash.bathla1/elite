import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { UserService } from './../../providers/user';
import { User } from './../../shared/user';

@IonicPage()
@Component({
  selector: 'page-account',
  templateUrl: 'account.html',
})
export class AccountPage {
  public user: User;
  constructor(
    public navCtrl: NavController, 
    public navParams: NavParams,
    public userService: UserService,
  ) {
    this.user = new User();
    if (navParams.get('user')) {
      this.user = navParams.get('user');
    }
  }

  logout(): void {
    this.userService.logout()
      // .subscribe(() => {
      //   this.navCtrl.setRoot('LoginPage');
      // })
  }

  gotoProfilePage(): void {
    this.navCtrl.push('ProfilePage', {user: this.user});
  }

  gotoSubscriptionPage(): void {
    this.navCtrl.push('SubscriptionPage', {user: this.user});
  }
  
  gotoIameliteaccountPage(): void {
    this.navCtrl.push('IameliteaccountPage', {user: this.user});
  }

  gotoFaqPage(): void {
    this.navCtrl.push('FaqPage', {user: this.user});
  }

  gotoHelpAndSupportPage(): void {
    this.navCtrl.push('HelpandsupportPage', {user: this.user});
  }

  gotoTermsAndPrivacyPage(): void {
    this.navCtrl.push('TermsAndPrivacyPage', {user: this.user});
  }

}
