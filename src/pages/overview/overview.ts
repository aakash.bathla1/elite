import { Component, ViewChild, ElementRef } from '@angular/core';
import { NavController, NavParams, IonicPage } from 'ionic-angular';
import { SafeResourceUrl} from '@angular/platform-browser';
import { DomSanitizer} from '@angular/platform-browser';
import { ContentService } from '../../providers/content';
import { UserService } from '../../providers/user';

@IonicPage()
@Component({
  selector: 'page-overview',
  templateUrl: 'overview.html',
})
export class OverviewPage {
  public weeks: any[];
  public currentWeek: number;
  @ViewChild('player') player: ElementRef;
  public video: SafeResourceUrl;
  public content: any;
  constructor(
    public navCtrl: NavController, 
    public navParams: NavParams,
    private contentService: ContentService,
    private userService: UserService,
    private domSanitizer: DomSanitizer,
  ) {
    this.currentWeek = this.userService.currentWeek;
    this.content = { content: '' };
  }

  ionViewWillEnter() {

    this.userService.getSelf()
      .subscribe(user => {
        this.weeks = user.weeks;
      });
    
    this.updateContent();
  }

  onCurrentWeekChanged(week: number) {
    this.currentWeek = week;
    this.updateContent();
  }

  private updateContent() {
    this.contentService.getWeeklyOverview(this.currentWeek)
      .subscribe(data => {
        this.content = data;
        this.content.video = this.domSanitizer.bypassSecurityTrustResourceUrl(this.content.source);
        this.content.preview = this.domSanitizer.bypassSecurityTrustResourceUrl(this.content.preview);
      });
  }

}

