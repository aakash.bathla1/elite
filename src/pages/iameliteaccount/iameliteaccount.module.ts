import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { IameliteaccountPage } from './iameliteaccount';

import { ComponentsModule } from '../../components/components.module';

@NgModule({
  declarations: [
    IameliteaccountPage,
  ],
  imports: [
    IonicPageModule.forChild(IameliteaccountPage),
    ComponentsModule
  ],
})
export class IameliteaccountPageModule {}
