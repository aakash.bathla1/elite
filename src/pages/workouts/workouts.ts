import { Component } from '@angular/core';
import { NavController, NavParams, IonicPage } from 'ionic-angular';
import { SafeResourceUrl, DomSanitizer} from '@angular/platform-browser';
import { ContentService } from '../../providers/content';
import { UserService } from '../../providers/user';

@IonicPage()
@Component({
  selector: 'page-workouts',
  templateUrl: 'workouts.html',
})
export class WorkoutsPage {
  public weeks: any[];
  public currentWeek: number;
  public currentDay: string;
  // @ViewChild('player') player: ElementRef;
  public video: SafeResourceUrl;
  public one: any;
  public two: any;
  public three: any;
  public four: any;
  public five: any;
  public six: any;
  public seven: any;
  constructor(
    public navCtrl: NavController, 
    public navParams: NavParams,
    public contentService: ContentService,
    public userService: UserService,
    public sanitizer: DomSanitizer,
  ) {
    this.currentWeek = this.userService.currentWeek;
    this.currentDay = "1"; // navParams.get('currentDay');
  }

  ionViewDidLoad() {
    this.userService.getSelf()
      .subscribe(user => {
        this.weeks = user.weeks;
      });
    this.updateContent();
  }

  onCurrentWeekChanged(week: number) {
    this.currentWeek = week;
    this.updateContent();
  }

  private updateContent() {
    this.contentService.getWeeklyDailyWorkout(this.currentWeek, 1)
      .subscribe(data => {
        this.one = data;
        this.one.src = this.sanitizer.bypassSecurityTrustResourceUrl(data.source);
        this.one.preview = this.sanitizer.bypassSecurityTrustResourceUrl(data.preview);
      });
    this.contentService.getWeeklyDailyWorkout(this.currentWeek, 2)
      .subscribe(data => {
        this.two = data;
        this.two.src = this.sanitizer.bypassSecurityTrustResourceUrl(data.source);
        this.two.preview = this.sanitizer.bypassSecurityTrustResourceUrl(data.preview);
      });
    this.contentService.getWeeklyDailyWorkout(this.currentWeek, 3)
      .subscribe(data => {
        this.three = data;
        this.three.src = this.sanitizer.bypassSecurityTrustResourceUrl(data.source);
        this.three.preview = this.sanitizer.bypassSecurityTrustResourceUrl(data.preview);
      });
    this.contentService.getWeeklyDailyWorkout(this.currentWeek, 4)
      .subscribe(data => {
        this.four = data;
        this.four.src = this.sanitizer.bypassSecurityTrustResourceUrl(data.source);
        this.four.preview = this.sanitizer.bypassSecurityTrustResourceUrl(data.preview);
      });
    this.contentService.getWeeklyDailyWorkout(this.currentWeek, 5)
      .subscribe(data => {
        this.five = data;
        this.five.src = this.sanitizer.bypassSecurityTrustResourceUrl(data.source);
        this.five.preview = this.sanitizer.bypassSecurityTrustResourceUrl(data.preview);
      });
    this.contentService.getWeeklyDailyWorkout(this.currentWeek, 6)
      .subscribe(data => {
        this.six = data;
        this.six.src = this.sanitizer.bypassSecurityTrustResourceUrl(data.source);
        this.six.preview = this.sanitizer.bypassSecurityTrustResourceUrl(data.preview);
      });
    this.contentService.getWeeklyDailyWorkout(this.currentWeek, 7)
      .subscribe(data => {
        this.seven = data;
        this.seven.src = this.sanitizer.bypassSecurityTrustResourceUrl(data.source);
        this.seven.preview = this.sanitizer.bypassSecurityTrustResourceUrl(data.preview);
      });
  }
}
