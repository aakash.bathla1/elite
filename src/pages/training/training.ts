import { Component, ViewChild, ElementRef } from '@angular/core';
import { NavController, NavParams, IonicPage } from 'ionic-angular';
import { DomSanitizer } from '@angular/platform-browser';

import { ContentService } from '../../providers/content';
import { UserService } from '../../providers/user';

@IonicPage()
@Component({
  selector: 'page-training',
  templateUrl: 'training.html',
})
export class TrainingPage {
  public weeks: any[];
  public currentWeek: number;
  @ViewChild('player') player: ElementRef;
  public content: any;
  constructor(
    public navCtrl: NavController, 
    public navParams: NavParams,
    private contentService: ContentService,
    private userService: UserService,
    private sanitizer: DomSanitizer,
  ) {
    this.currentWeek = this.userService.currentWeek;
    this.content = { content: '' };
  }

  ionViewWillEnter() {
    this.userService.getSelf()
      .subscribe(user => {
        this.weeks = user.weeks;
      });
    this.updateContent();
  }

  onCurrentWeekChanged(week: number) {
    this.currentWeek = week;
    this.updateContent();
  }

  private updateContent() {
    this.contentService.getWeeklyTrainingSplit(this.currentWeek)
      .subscribe(data => {
        this.content = data;
        this.content.videoSource = this.content.imageSource = this.sanitizer.bypassSecurityTrustResourceUrl(data.source);
      });
  }
}
