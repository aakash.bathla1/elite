import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { ElitePage } from './elite';

import { ComponentsModule } from '../../components/components.module';

@NgModule({
  declarations: [
    ElitePage,
  ],
  imports: [
    IonicPageModule.forChild(ElitePage),
    ComponentsModule,
  ],
})
export class ElitePageModule {}
