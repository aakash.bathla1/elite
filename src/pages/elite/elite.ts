import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { DomSanitizer} from '@angular/platform-browser'

import { UserService } from './../../providers/user';
import { ContentService } from '../../providers/content';

@IonicPage()
@Component({
  selector: 'page-elite',
  templateUrl: 'elite.html',
})
export class ElitePage {
  public contents: any[];
    constructor(
      public navCtrl: NavController, 
      public navParams: NavParams,
      public contentService: ContentService,
      public domSanitizer: DomSanitizer,
      public userService: UserService,
    ) {
      this.contents = [];
    }

  ionViewDidLoad() {
    this.contentService
      .getElite()
      .subscribe(data => {
        this.contents = data.map(content => {
          content.styleUrl = this.domSanitizer.bypassSecurityTrustStyle('url(' + content.preview + ')');
          return content;
        })
      })
      ;
  }

  viewPage(content): void {
    this.navCtrl.push('EliteDetailPage', {content: content});
  }

}