import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';

@IonicPage()
@Component({
  selector: 'page-terms-and-privacy',
  templateUrl: 'terms-and-privacy.html',
})
export class TermsAndPrivacyPage {
  constructor(public navCtrl: NavController, public navParams: NavParams) {
  }

}
