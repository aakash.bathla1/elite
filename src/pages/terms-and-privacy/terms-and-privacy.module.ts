import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { TermsAndPrivacyPage } from './terms-and-privacy';

import { ComponentsModule } from '../../components/components.module';

@NgModule({
  declarations: [
    TermsAndPrivacyPage,
  ],
  imports: [
    IonicPageModule.forChild(TermsAndPrivacyPage),
    ComponentsModule,
  ],
})
export class TermsAndPrivacyPageModule {}
