import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { HelpandsupportPage } from './helpandsupport';

import { ComponentsModule } from '../../components/components.module';

@NgModule({
  declarations: [
    HelpandsupportPage,
  ],
  imports: [
    IonicPageModule.forChild(HelpandsupportPage),
    ComponentsModule
  ],
})
export class HelpandsupportPageModule {}
