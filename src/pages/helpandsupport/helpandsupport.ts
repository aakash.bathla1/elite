import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';

@IonicPage()
@Component({
  selector: 'page-helpandsupport',
  templateUrl: 'helpandsupport.html',
})
export class HelpandsupportPage {
  constructor(public navCtrl: NavController, public navParams: NavParams) {
  }

}
