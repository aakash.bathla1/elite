import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';

@IonicPage()
@Component({
  selector: 'page-accountability',
  templateUrl: 'accountability.html',
})
export class AccountabilityPage {
  constructor(public navCtrl: NavController, public navParams: NavParams) {
  }

}
