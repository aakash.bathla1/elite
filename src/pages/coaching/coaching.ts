import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';

@IonicPage()
@Component({
  selector: 'page-coaching',
  templateUrl: 'coaching.html',
})
export class CoachingPage {
  constructor(public navCtrl: NavController, public navParams: NavParams) {
  }

}
