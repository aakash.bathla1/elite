import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { UpgradecancelPage } from './upgradecancel';

@NgModule({
  declarations: [
    UpgradecancelPage,
  ],
  imports: [
    IonicPageModule.forChild(UpgradecancelPage),
  ],
})
export class UpgradecancelPageModule {}
