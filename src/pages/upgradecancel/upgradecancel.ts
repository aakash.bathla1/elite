import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';

@IonicPage()
@Component({
  selector: 'page-upgradecancel',
  templateUrl: 'upgradecancel.html',
})
export class UpgradecancelPage {
  constructor(public navCtrl: NavController, public navParams: NavParams) {
  }

}
