import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import * as $ from 'jquery';

@IonicPage()
@Component({
  selector: 'page-faq',
  templateUrl: 'faq.html',
})
export class FaqPage {
  constructor(public navCtrl: NavController, public navParams: NavParams) {
  }

  ionViewWillEnter() {
  	$(document).ready(function(){
  		$('.single-faq .title').on('click', function(){
  			$(this).next().slideToggle();
  			$(this).toggleClass('active');
  		});
  	});
  }

}
