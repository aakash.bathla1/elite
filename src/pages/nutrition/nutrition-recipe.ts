import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { DomSanitizer} from '@angular/platform-browser'

import { UserService } from './../../providers/user';
import { ContentService } from '../../providers/content';
import { Content } from './../../shared/content';

@IonicPage()
@Component({
  selector: 'page-nutrition-recipe',
  templateUrl: 'nutrition-recipe.html',
})
export class NutritionRecipePage {
  public contents: Content[];
  constructor(
    public navCtrl: NavController, 
    public navParams: NavParams,
    public contentService: ContentService,
    public domSanitizer: DomSanitizer,
    public userService: UserService,
  ) {
    this.contents = [];
  }

  ionViewWillEnter() {
    this.updateContent();

    this.userService.currentWeek$
      .subscribe(week => this.updateContent())
  }

  updateContent(): void {
    this.contentService
    .getWeeklyRecipes(this.userService.currentWeek)
    .subscribe(data => {
      this.contents = data.map(content => {
        content.styleUrl = this.domSanitizer.bypassSecurityTrustStyle('url(' + content.preview + ')');
        return content;
      })
    })
  }

  viewPage(content): void {
    this.navCtrl.push('NutritionRecipeDetailPage', {content: content});
  }

  gotoHomePage(): void {
    this.userService.resetNavigationTo('home');
  }

}
