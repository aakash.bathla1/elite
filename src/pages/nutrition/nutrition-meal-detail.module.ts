import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { NutritionMealDetailPage } from './nutrition-meal-detail';

import { ComponentsModule } from '../../components/components.module';


@NgModule({
  declarations: [
    NutritionMealDetailPage,
  ],
  imports: [
    IonicPageModule.forChild(NutritionMealDetailPage),
    ComponentsModule,
  ],
})
export class NutritionMealDetailPageModule {}
