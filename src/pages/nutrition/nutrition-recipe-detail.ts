import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { DomSanitizer} from '@angular/platform-browser';

import { Content } from './../../shared/content';

@IonicPage()
@Component({
  selector: 'page-nutrition-recipe-detail',
  templateUrl: 'nutrition-recipe-detail.html',
})
export class NutritionRecipeDetailPage {
  public content: Content;
  constructor(
    public navCtrl: NavController, 
    public navParams: NavParams,
    private domSanitizer: DomSanitizer,
  ) {
    this.content = navParams.get('content');
  }

  ionViewDidLoad() {
    this.content.imageSource = this.domSanitizer.bypassSecurityTrustResourceUrl(this.content.source);
    this.content.videoSource = this.domSanitizer.bypassSecurityTrustResourceUrl(this.content.source);
  }

}
