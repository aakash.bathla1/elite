import { Component } from '@angular/core';
import { NavController, NavParams, IonicPage } from 'ionic-angular';

import { ContentService } from '../../providers/content';
import { UserService } from '../../providers/user';
@IonicPage()
@Component({
  selector: 'page-nutrtion',
  templateUrl: 'nutrition.html',
})
export class NutritionPage {
  public mealRoot: any = 'NutritionMealPage';
  public recipeRoot: any = 'NutritionRecipePage';
  
  public weeks: any[];
  public currentWeek: number;
  public type: string;
  public mealPlan: any;
  public recipes: any;
  constructor(
    public navCtrl: NavController, 
    public navParams: NavParams,
    public contentService: ContentService,
    public userService: UserService
  ) {
    this.currentWeek = this.userService.currentWeek;
    this.type = "meal_plan";
  }
}
