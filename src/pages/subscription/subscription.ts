import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, LoadingController, Loading, ToastController } from 'ionic-angular';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';

import { User } from './../../shared/user';
import { UserService } from './../../providers/user';

@IonicPage()
@Component({
  selector: 'page-subscription',
  templateUrl: 'subscription.html',
})
export class SubscriptionPage {
  public user: User;
  public loader: Loading;
  public form: FormGroup;

  constructor(
    public navCtrl: NavController, 
    public navParams: NavParams,
    private userService: UserService,
    private loadingCtrl: LoadingController,
    private toastCtrl: ToastController,
    fb: FormBuilder
  ) {
    this.user = new User({subscription: {status: 'active'}});
    this.userService.getSelf()
      .subscribe((user: User) => this.user = user);
    
    this.form = fb.group({
      number: ['', Validators.required],
      month: ['', Validators.required],
      year: ['', Validators.required],
      cvc: ['', Validators.required],
    });
  }

  cancelSubscription(): void {
    console.log("cancelSubscription..");
    this.userService.cancelSubscription()
      .subscribe((user) => {
        this.loader.dismiss();
        let info = this.toastCtrl.create({
          message: 'Your subscription has been cancelled. You have '+user.subscription.days_left+' days left in your subscription to use the app.',
          position: 'top',
          duration: 5000
        });
        info.present().then(() => this.navCtrl.pop());
      }, error => {
        this.loader.dismiss();
        let info = this.toastCtrl.create({
          message: error.message,
          position: 'top',
          duration: 5000
        });
        info.present();
      });

    this.loader = this.loadingCtrl.create();
    this.loader.present();
  }

  updateCard(): void {
    console.log("updateCard..");
    this.userService.validateCard(this.form.value)
    .subscribe(() => {
      this.loader.dismiss();
      let info = this.toastCtrl.create({
        message: 'We have updated your payment details successfully!',
        position: 'top',
        duration: 3000
      });
      info.present().then(() => this.navCtrl.pop());
    }, error => {
      this.loader.dismiss();
      
      let info = this.toastCtrl.create({
        message: error.message,
        position: 'top',
        duration: 5000
      });
      info.present();
    });
    this.loader = this.loadingCtrl.create();
    this.loader.present();
  }

}
