import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { SubscriptionPage } from './subscription';

import { ComponentsModule } from '../../components/components.module';

@NgModule({
  declarations: [
    SubscriptionPage,
  ],
  imports: [
    IonicPageModule.forChild(SubscriptionPage),
    ComponentsModule
  ],
})
export class SubscriptionPageModule {}
