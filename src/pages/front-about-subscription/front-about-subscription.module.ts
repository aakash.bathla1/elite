import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { FrontAboutSubscriptionPage } from './front-about-subscription';

@NgModule({
  declarations: [
    FrontAboutSubscriptionPage,
  ],
  imports: [
    IonicPageModule.forChild(FrontAboutSubscriptionPage),
  ],
})
export class FrontAboutSubscriptionPageModule {}
