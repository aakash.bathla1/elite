import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { SubscriptionPromptPage } from './subscription-prompt';

import { ComponentsModule } from '../../components/components.module';

@NgModule({
  declarations: [
    SubscriptionPromptPage,
  ],
  imports: [
    IonicPageModule.forChild(SubscriptionPromptPage),
    ComponentsModule,
  ],
})
export class SubscriptionPromptPageModule {}
