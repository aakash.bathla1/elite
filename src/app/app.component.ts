import { Component } from '@angular/core';
import { Platform, App, Events, LoadingController, Loading, ToastController } from 'ionic-angular';
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
import { InAppPurchase2 } from '@ionic-native/in-app-purchase-2';
import { Storage } from '@ionic/storage';

import { UserService } from './../providers/user';

@Component({
  templateUrl: 'app.html'
})
export class MyApp {
  rootPage: any = 'LoginPage';
  public loader: Loading;
  public unlimited: string;
  public weekly: string;
  constructor(
    platform: Platform,
    statusBar: StatusBar,
    splashScreen: SplashScreen,
    userService: UserService,
    app: App,
    events: Events,
    store: InAppPurchase2,
    storage: Storage,
    loadingCtrl: LoadingController,
    toastCtrl: ToastController
  ) {
    events.subscribe('user:unauthorized', () => {
      const rootNavs = app.getRootNavs();
      if (rootNavs.length) {
        rootNavs[0].goToRoot();
      }
    })

    userService.rootPageNotifier.subscribe(page => {
      const rootNavs = app.getRootNavs();
      console.log('MyApp ::userService.rootPageNotifier::', page)
      let nextPage = '';
      switch (page) {
        case 'login':
          nextPage = 'LoginPage';
          break;
        case 'home':
          nextPage = 'HomePage';
          break;
        case 'subscription':
          nextPage = 'SubscriptionPromptPage';
          break;
        default:
          nextPage = 'LoginPage';
      }

      console.log('AppComponent :: ', nextPage, this.rootPage);

      // if (nextPage === this.rootPage) return;

      this.rootPage = nextPage;

      if (rootNavs.length) {
        rootNavs[0].goToRoot();
      }
    })

    platform.ready().then(() => {
      console.log('MyApp ::ready.. ')
      if (platform.is('cordova')) {
        console.log('MyApp ::ready::cordova platform ')
        statusBar.overlaysWebView(false);
        statusBar.styleLightContent();
        statusBar.backgroundColorByName('black');
        splashScreen.hide();

        this.loader = loadingCtrl.create({ dismissOnPageChange: true });

        try {
          // check for both products -- unlimited and weekly
          store.verbosity = store.DEBUG

          if (platform.is('ios')) {
            console.log('ready::subscription :: ios')
            this.unlimited = 'com.jaypiggin.iamelite.unlimited1';
            this.weekly = 'com.jaypiggin.iamelite.weekly3';

            store.register({
              id: this.unlimited,
              alias: this.unlimited,
              type: store.NON_RENEWING_SUBSCRIPTION
            });

            store.register({
              id: this.weekly,
              alias: this.weekly,
              type: store.PAID_SUBSCRIPTION
            });

            console.log('end - ready::subscription ::android::REGISTER PRODUCTS ')

            console.log('Products: ', JSON.stringify(store.products));

            // console.log('SubscriptionPromptPage[android]::registrationCalls::222store weeklyProduct..', JSON.stringify(store.get("weekly_subscription")));
            // console.log('SubscriptionPromptPage[android]::registrationCalls::222store unlimitedsub..', JSON.stringify(store.get("unlimited_subscription")));
            // console.log('SubscriptionPromptPage[android]::registrationCalls::222store testsubscription..', JSON.stringify(store.get("testsubscription")));

            console.log('START - AppComponent ::And Refresh Store');
            store.refresh(); //UPDATE PRODUCTS FROM STORE
            console.log('END - AppComponent ::And Refresh Store');

            console.log('Products:after refresh ', JSON.stringify(store.products));

            // // we dont auto restore. should have been explicitly restored from subscription page.
            // // we have this key stored from there.
            // storage.get('subscription').then(subscription => {
            //   console.log('subscription :: ' + JSON.stringify(subscription))
            //   if (subscription && subscription.weekly) {
            //     store.once('weekly').approved((product: IAPProduct) => {
            //       console.log('AppComponent :: Approved ' + JSON.stringify(product));
            //       userService.resetNavigationTo('login');
            //     });
            //   }

            //   if (subscription && subscription.unlimited) {
            //     store.once('unlimited').approved((product: IAPProduct) => {
            //       console.log('AppComponent :: Approved ' + JSON.stringify(product));
            //       userService.resetNavigationTo('login')
            //     });
            //   }
            //   console.log('AppComponent ::IOS Refresh Store.');
            //   store.refresh();
            // })
          }
          else {
            //android code
            console.log('ready::subscription ::android ')
            // this.unlimited = 'unlimited_subscription';
            // this.weekly = 'weekly_subscription';

            console.log('start - ready::subscription ::android::REGISTER PRODUCTS ')
            store.register({
              id: "unlimited_subscription",
              alias: "unlimited subscription",
              type: store.NON_RENEWING_SUBSCRIPTION
            });

            store.register({
              id: "weekly_subscription",
              alias: "weekly-subscription",
              type: store.PAID_SUBSCRIPTION
            });

            // store.register({
            //   id: "testsubscription",
            //   alias: "testsubscription_alias",
            //   type: store.PAID_SUBSCRIPTION
            // });


            // store.register({
            //   id: "wrongsubscription",
            //   alias: "wrongsubscription_alias",
            //   type: store.PAID_SUBSCRIPTION
            // });
            console.log('end - ready::subscription ::android::REGISTER PRODUCTS ')

            console.log('Products: ', JSON.stringify(store.products));

            // console.log('SubscriptionPromptPage[android]::registrationCalls::222store weeklyProduct..', JSON.stringify(store.get("weekly_subscription")));
            // console.log('SubscriptionPromptPage[android]::registrationCalls::222store unlimitedsub..', JSON.stringify(store.get("unlimited_subscription")));
            // console.log('SubscriptionPromptPage[android]::registrationCalls::222store testsubscription..', JSON.stringify(store.get("testsubscription")));

            console.log('START - AppComponent ::And Refresh Store');
            store.refresh(); //UPDATE PRODUCTS FROM STORE
            console.log('END - AppComponent ::And Refresh Store');

            console.log('Products:after refresh ', JSON.stringify(store.products));
            // ASMOD 20APRIL18
            // // we dont auto restore. should have been explicitly restored from subscription page.
            // // we have this key stored from there.
            // storage.get('subscription').then(subscription => {
            //   console.log('************  START - SUBSCRIPTION FOUND IN STORAGE ************')
            //   console.log('subscription from storage:: ' + JSON.stringify(subscription))
            //   if (subscription && subscription.weekly) {
            //     console.log('******  WEEKLY SUBSCRIPTION FOUND IN STORAGE ******')
            //     store.once('weekly_subscription').approved((product: IAPProduct) => {
            //       console.log('AppComponent :: Approved ' + JSON.stringify(product));
            //       userService.resetNavigationTo('login');
            //     });
            //   }
            //   else {
            //     console.warn('MyApp ::ready::no weekly subscription found in storage')
            //   }

            //   if (subscription && subscription.unlimited) {
            //     console.log('****  unlimited SUBSCRIPTION FOUND IN STORAGE *****')
            //     store.once('unlimited_subscription').approved((product: IAPProduct) => {
            //       console.log('AppComponent :: Approved ' + JSON.stringify(product));
            //       userService.resetNavigationTo('login')
            //     });
            //   }
            //   else {
            //     console.warn('MyApp ::ready::no unlimited subscription found in storage')
            //   }
            //   console.log('************ END - SUBSCRIPTION FOUND IN STORAGE ************')
            // })







          }




          // store.ready(true).then((status) => {
          //   console.log('#########################################Store is Ready: ' + JSON.stringify(status));
          //   console.log('Products: ' + JSON.stringify(store.products));
          // }).catch(function onCatch(response) {
          //   console.error('Store ready error:', JSON.stringify(response));
          // });




          store.error(err => {
            console.error('AppComponent :: error', JSON.stringify(err))
            this.loader.dismiss();
            let info = toastCtrl.create({
              message: 'Cannot connect to app store. Please check if you are signed in, reopen the app and try again.',
              position: 'top',
              duration: 3000
            });
            info.present();
          })





          console.log('Products: ', JSON.stringify(store.products));

          // console.log('SubscriptionPromptPage[android]::registrationCalls::333 weeklyProduct..', JSON.stringify(store.get("weekly_subscription")));
          // console.log('SubscriptionPromptPage[android]::registrationCalls::333 unlimitedsub..', JSON.stringify(store.get("unlimited_subscription")));
          // console.log('SubscriptionPromptPage[android]::registrationCalls::333 testsubscription..', JSON.stringify(store.get("testsubscription")));












        } catch (err) {
          console.error('Store error: ' + JSON.stringify(err))
          this.loader.dismiss();
          let info = toastCtrl.create({
            message: 'Cannot connect to app store. Please check if you are signed in, reopen the app and try again.',
            position: 'top',
            duration: 3000
          });
          info.present();
        }
      }
    });
  }
}

