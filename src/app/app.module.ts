import { FileTransfer } from '@ionic-native/file-transfer';
import { BrowserModule } from '@angular/platform-browser';
import { ErrorHandler, NgModule } from '@angular/core';
import { HttpModule } from '@angular/http';
import { IonicApp, IonicErrorHandler, IonicModule } from 'ionic-angular';
import { SplashScreen } from '@ionic-native/splash-screen';
import { StatusBar } from '@ionic-native/status-bar';
import { IonicStorageModule } from '@ionic/storage';
import { Camera } from '@ionic-native/camera';
import { InAppPurchase2 } from '@ionic-native/in-app-purchase-2';

import { MyApp } from './app.component';
import { Config } from '../providers/config';
import { UserService } from '../providers/user';
import { ContentService } from '../providers/content';

import { ComponentsModule } from '../components/components.module';

@NgModule({
  declarations: [
    MyApp
  ],
  imports: [
    BrowserModule,
    HttpModule,
    IonicStorageModule.forRoot(),
    IonicModule.forRoot(MyApp),
    ComponentsModule
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp
  ],
  providers: [
    InAppPurchase2,
    FileTransfer,
    Camera,
    StatusBar,
    SplashScreen,
    {provide: ErrorHandler, useClass: IonicErrorHandler},
    Config,
    UserService,
    ContentService
  ]
})
export class AppModule {}
