import { Headers, Http } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import { Events } from 'ionic-angular';
import 'rxjs/add/observable/of';
import 'rxjs/add/observable/fromPromise';
import 'rxjs/add/observable/throw';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/mergeMap';
import 'rxjs/add/operator/catch';

import { Config } from './config';

export class BaseService {
  protected headers: Headers;

  constructor(
    protected http: Http,
    protected config: Config,
    protected events: Events
  ) {
    this.headers = new Headers({
      'Content-Type': 'application/json',
      'Accept': 'application/json',
      'Authorization': 'Bearer ' + config.ACCESS_TOKEN
    });
  }

  get(endpoint: string, options?: any) : Observable<any> {
    this.headers.set('Authorization', 'Bearer ' + this.config.ACCESS_TOKEN);
    options = options || { headers: this.headers };

    return this.http
      .get(this.config.API_URL+ endpoint, options)
      .catch((error: Response | any) => {
        let errMsg: string = 'Something went wrong!';
        const body:any = error.json() || '';
        if (body.message) {
          errMsg = body.message;
        }
        let errCode = 'unknown_error';
        if (body.code) {
          errCode = body.code;
        }
        console.error('catched error', error.status, errCode, errMsg);
        if (error.status === 401 || error.status === 403) {
          console.error('error.status === 401 || error.status === 403');
          this.events.publish('user:unauthorized');
        }
        return Observable.throw({code: errCode, message: errMsg});
      })
  }

  post(endpoint: string, data:any, options?: any) : Observable<any> {
    this.headers.set('Authorization', 'Bearer ' + this.config.ACCESS_TOKEN);
    options = options || { headers: this.headers };
    
    return this.http
      .post(this.config.API_URL + endpoint, data, options)
      .catch((error: Response | any) => {
        let errMsg: string = 'Something went wrong!';
        const body:any = error.json() || '';
        if (body.message) {
          errMsg = body.message;
        }
        let errCode = 'unknown_error';
        if (body.code) {
          errCode = body.code;
        }
        console.log('ERROR :: ', error.status, errCode, errMsg);
        if (error.status === 401 || error.status === 403) {
          this.events.publish('user:unauthorized');
        }
        return Observable.throw({code: errCode, message: errMsg});
      })
  }
}