import { Injectable } from '@angular/core';
import { Storage } from '@ionic/storage';

@Injectable()
export class Config {
  public API_URL: string;
  public ACCESS_TOKEN: string; // for quick access instead of storage
  public CLIENT_ID: string = '4';
  public CLIENT_SECRET: string = 'ykSjaVayfXY67fasDBFrsx0ytgKVWcNT30cbpQw9';
  constructor(storage: Storage) {
    // this.API_URL = 'http://localhost:8080';
    this.API_URL = 'http://138.197.108.34/';
    storage.get('user')
      .then(user => {
        if (user && user.accessToken) this.ACCESS_TOKEN = user.accessToken;
      });
  }
}
