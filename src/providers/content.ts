import { Injectable } from '@angular/core';
import { Events } from 'ionic-angular';
import { Http } from '@angular/http';
import { Observable } from 'rxjs/Observable';

import { Config } from './config';
import { BaseService } from './base';
import { Content } from './../shared/content';

@Injectable()
export class ContentService extends BaseService {
  constructor(
    protected http: Http, 
    protected config: Config,
    protected events: Events
  ) {
    super(http, config, events);
  }

  getWeeklyOverview(week: number): Observable<any> {
    return this.get('/api/week/' + week + '/overview')
      .map(response => response.json().data)
  }
  
  getWeeklyTrainingSplit(week: number): Observable<any> {
    return this.get('/api/week/' + week + '/training-split')
      .map(response => response.json().data)
  }

  getWeeklyDailyWorkout(week: number, day: number): Observable<any> {
    return this.get('/api/week/' + week + '/workouts/' + day)
      .map(response => response.json().data)
  }

  getWeeklyRecipes(week: number): Observable<Content[]> {
    return this.get('/api/week/' + week + '/recipes')
      .map(response => {
        return response.json().data.map(content => new Content(content));
      })
  }

  getWeeklyMealPlans(week: number): Observable<Content[]> {
    return this.get('/api/week/' + week + '/meal-plan')
      .map(response => {
        return response.json().data.map(content => new Content(content));
      })
  }

  getEducation(type: string): Observable<any[]> {
    return this.get('/api/education/' + type)
      .map(response => response.json().data)
  }

  getExerciseDemos(): Observable<any[]> {
    return this.get('/api/exercise-demos')
      .map(response => response.json().data)
  }

  getElite(): Observable<any[]> {
    return this.get('/api/elite')
      .map(response => response.json().data)
  }

}
