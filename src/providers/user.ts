import { Injectable } from '@angular/core';
import { Events } from 'ionic-angular';
import { Http, Headers } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import { Subject } from 'rxjs/Subject';
import { ReplaySubject } from 'rxjs/ReplaySubject';
import { Storage } from '@ionic/storage';

import { Config } from './config';
import { BaseService } from './base';
import { User } from './../shared/user';

@Injectable()
export class UserService extends BaseService {
  public currentUser: any;
  public currentWeek: number;
  private currentWeekSource = new Subject<number>();
  public currentWeek$ = this.currentWeekSource.asObservable();
  private rootPageNotification = new ReplaySubject<string>(1);
  public rootPageNotifier = this.rootPageNotification.asObservable();
  constructor(
    protected http: Http, 
    private storage: Storage,
    protected config: Config,
    protected events: Events
  ) {
    super(http, config, events);
    this.currentWeek = 0;
  }

  login(email: string, password: string): Observable<any> {
    this.storage.remove('user');
    return this.post('/oauth/token', {
      grant_type: 'password',
      client_id: this.config.CLIENT_ID,
      client_secret: this.config.CLIENT_SECRET,
      username: email,
      password: password
    })
    .mergeMap(response => {
      let data = response.json();
      
      this.config.ACCESS_TOKEN = data.access_token;
      this.headers.set('Authorization', 'Bearer ' + data.access_token);
      
      return this.getSelf(true);
    })
  }

  logout(): Observable<any> {
    this.config.ACCESS_TOKEN = null;
    this.rootPageNotification.next('login');
    return Observable.fromPromise(this.storage.remove('user'));
  }

  getSelf(refetch?: boolean): Observable<any> {
    if (refetch) {
      return this.get('/api/me')
        .map(response => {
          let user = new User(response.json().data);
          user.accessToken = this.config.ACCESS_TOKEN; // save back the token
          this.currentUser = user;
          this.storage.set('user', user);
          return user;
        })
    }

    return Observable.fromPromise(this.storage.get('user'))
      .mergeMap(user => {
        if (user) {
          return Observable.of(user)
        } else {
          return this.get('/api/me')
          .map(response => {
            let user = new User(response.json().data);
            user.accessToken = this.config.ACCESS_TOKEN; // save back the token
            this.currentUser = user;
            this.storage.set('user', user);
            return user;
          })
        }
      })
      ;
  }

  updateProfile(data: any): Observable<any> {
    let postData: any =  {
      name: data.name,
      email: data.email,
      height: data.height,
      current_weight: data.currentWeight,
      starting_weight: data.startingWeight,
      goal_weight: data.goalWeight,
      age: data.age,
      timezone: data.timezone
    };

    if (data.showGettingStarted === false) {
      postData.getting_started_done = true;
    }
    
    return this.post('/api/user', postData)
    .map(response => {
      let user = new User(response.json().data);
      user.accessToken = this.config.ACCESS_TOKEN; // save back the token
      this.currentUser = user;
      this.storage.set('user', user);
      return user;
    })
  }

  updateCurrentWeek(week: number) {
    this.currentWeek = week;
    this.currentWeekSource.next(week);
  }

  uploadPhoto(file: File): Observable<any> {
    // coz we need not the content-type
    const headers = new Headers({
      // 'Content-Type': 'application/json',
      'Accept': 'application/json',
      'Authorization': 'Bearer ' +this.config.ACCESS_TOKEN
    });
    let formData: FormData = new FormData();
    formData.append('photo', file, file.name);
    return this.post('/api/user/upload-photo', formData, {headers: headers})
      .map(response => {
        let user = new User(response.json().data);
        user.accessToken = this.config.ACCESS_TOKEN; // save back the token
        this.currentUser = user;
        this.storage.set('user', user);
        return user;
      });
  }

  uploadProgressPhoto(file: File, week: number): Observable<any> {
    // coz we need not the content-type
    const headers = new Headers({
      // 'Content-Type': 'application/json',
      'Accept': 'application/json',
      'Authorization': 'Bearer ' +this.config.ACCESS_TOKEN
    });
    let formData: FormData = new FormData();
    formData.append('photo', file, file.name);
    return this.post('/api/user/progresses/' + week + '/upload-photo', formData, {headers: headers})
      .map(response => response.json().data)
  }

  getProgress(): Observable<any[]> {
    return this.get('/api/user/progresses')
      .map(response => response.json().data)
  }

  sendPasswordResetLink(email: string): Observable<any> {
    return this.post('/api/send-password-reset-link', { email: email })
  }

  renewSubscription(resetNav ?: boolean): Observable<any> {
    return this.post('/api/user/renew-subscription', {})
      .map(response => {
        let user = new User(response.json().data);
        user.accessToken = this.config.ACCESS_TOKEN; // save back the token
        this.currentUser = user;
        this.storage.set('user', user);
        if (resetNav) this.resetNavigationTo('home');
        return user;
      })
  }

  cancelSubscription(): Observable<any> {
    return this.post('/api/user/cancel-subscription', {})
      .map(response => {
        let user = new User(response.json().data);
        user.accessToken = this.config.ACCESS_TOKEN; // save back the token
        this.currentUser = user;
        this.storage.set('user', user);
        return user;
      })
  }

  validateCard(data: any, subscribe ?: boolean): Observable<any> {
    return this.post('/api/user/validate-card', {
      number: data.number,
      month: data.month,
      year: data.year,
      cvc: data.cvc,
      subscribe: subscribe
    })
    .map(response => {
      let user = new User(response.json().data);
      user.accessToken = this.config.ACCESS_TOKEN; // save back the token
      this.currentUser = user;
      this.storage.set('user', user);
      return user;
    })
  }

  resetNavigationTo(page: string) : void {
    this.rootPageNotification.next(page);
  }

  preRegister(data: any) : Observable<any> {
    return Observable.fromPromise(this.storage.set('newUser', data))
      .mergeMap(() => {
        return this.post('/api/user/pre-register', data)
      })
  }

  registerNewProfile() : Observable<User> {
    console.log('UserService :: registerNewProfile');
    return Observable.fromPromise(this.storage.get('newUser'))
      .mergeMap(newUser => {
        if (!newUser) return Observable.of(newUser);
        this.storage.remove('newUser');
        return this.post('/api/user/register', newUser)
      })
  }
}
