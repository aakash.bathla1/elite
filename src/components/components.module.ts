import { NgModule } from '@angular/core';
import { IonicModule } from 'ionic-angular';
import { ProfilebarComponent } from './profilebar/profilebar';
@NgModule({
	declarations: [ProfilebarComponent],
	imports: [IonicModule],
	exports: [ProfilebarComponent]
})
export class ComponentsModule {}
